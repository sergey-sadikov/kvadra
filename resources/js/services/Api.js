import axios from 'axios'
import config from '../config'

export default() => {
    return axios.create({
        baseURL: config.api.baseUrl,
        withCredentials: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
}