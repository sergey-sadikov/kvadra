import Api from './Api'
import L from "leaflet";

export default {
    getMarkers() {
        return Api().get('markers');
    },
    storeMarker(data) {
        return Api().post('markers', data);
    },
    createLMarker(marker, category) {
        let markerOptions = {
            'created_at': marker.created_at
        };
        if(category) {
            Object.assign(markerOptions, {
                'category_id': category.id,
                'category_name': category.name
            })
        }
        let latLng = L.latLng(marker.lat, marker.lon);
        let lMarker = L.marker(latLng, markerOptions);
        if(category) lMarker = this.lMarkerBindInfoPupup(lMarker);
        return lMarker;
    },
    lMarkerBindInfoPupup(lMarker) {
        let popupContainer = document.createElement('div');
        let category = document.createElement('p');
        let createdDate = document.createElement('p');
        category.innerText = lMarker.options.category_name;
        category.classList.add('map__popup-title');
        createdDate.innerText = lMarker.options.created_at;
        popupContainer.appendChild(category);
        popupContainer.appendChild(createdDate);
        lMarker.bindPopup(popupContainer.outerHTML);
        return lMarker;
    }
}