import CategoryService from '../services/CategoryService'
import MarkerService from '../services/MarkerService'

let actions = {
    init({commit, dispatch}) {
        CategoryService.getCategories().then(response => commit('setCategories', response.data));
        MarkerService.getMarkers().then(response => dispatch('createLMarkers', {markers: response.data, append: false}));
    },
    createLMarkers({state, commit}, payload) {
        let lMarkers = [];
        console.log(payload);
        payload.markers.map(marker => {
            let category = state.categories.find(category => category.id === Number(marker.category_id));
            if(category) {
                let lMarker = MarkerService.createLMarker(marker, category);
                lMarkers.push(lMarker);
            }
        });
        if(payload.append === true) {
            lMarkers = state.markers.concat(lMarkers)
        }
        commit('setMarkers', lMarkers);
    }
};
export default actions