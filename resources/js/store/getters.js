let getters = {
    getMarkersByCategoryId: (state) => (categoryId) => {
        let markers = state.markers;
        let categories = state.categories;
        if(Number.isInteger(categoryId)) {
            let category = categories.find(category => category.id === categoryId);
            if(category) {
                markers = markers.filter(marker => marker.options.category_id === categoryId);
            }
        }
        return markers;
    },
    categorySelectOptions: (state) => {
        let options = [];
        let categories = state.categories;
        categories.map(category => options.push({
            text: category.name,
            value: category.id
        }));
        return options;
    }
};
export default getters;