import Vue from 'vue'
import VueRouter from 'vue-router'
import MapPage from './components/pages/MapPage.vue'
import MarkersPage from './components/pages/MarkersPage.vue'

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {path: '', redirect: '/map'},
        {path: '/map', component: MapPage},
        {path: '/markers', component: MarkersPage}
    ],
    linkActiveClass: 'active'
});

export default router