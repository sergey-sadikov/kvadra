const config = {
    api: {
        baseUrl: '/api/'
    },
    map:
        {
            apiUrl: 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
            layers: {
                mapbox: {
                    id: 'mapbox.streets',
                    accessToken: 'pk.eyJ1Ijoid2hpdGVjb2RlIiwiYSI6ImNqbHJkenJnODAwOHMzcHBmZmd1eW8zOXMifQ.Bh7SM4Vv6l8cNp6HAZcNQA'
                }
            },
            options: {
                center: [59.939095, 30.315868],
                minZoom: 2,
                zoom: 11,
                zoomControl: true
            }
        }
};
export default config