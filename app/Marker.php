<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{
    protected $fillable = ['category_id', 'lat', 'lon'];
    const UPDATED_AT = null;
}
