<?php

namespace App\Http\Controllers;

use App\Marker;
use App\Category;
use Illuminate\Http\Request;

class MarkerController extends Controller
{
    public function getMarkersJson() {
        $markers = Marker::all()->toJson();
        return $markers;
    }

    public function store(Request $request)
    {
        $marker = Marker::create([
            'lat' => $request->get('lat'),
            'lon' => $request->get('lon'),
            'category_id' => $request->get('category_id')
        ]);
        $marker->save();
        return $marker->toJson();
    }
}
