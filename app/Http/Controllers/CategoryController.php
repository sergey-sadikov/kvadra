<?php

namespace App\Http\Controllers;

use App\Category;

class CategoryController extends Controller
{
    public function getCategoriesJson() {
        $categories = Category::all()->toJson();
        return $categories;
    }
}
