<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        $data = [];
        for($i = 0; $i < 30; $i++)
        {
            $data[] = [
              'name' => $faker->company
            ];
        }
        DB::table('categories')->insert($data);
    }
}
