<?php

use App\Category;
use Illuminate\Database\Seeder;

class MarkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        $categoryIds = Category::select('id')->get()->pluck('id');
        $data = [];
        for($i = 0; $i < 150; $i++)
        {
            $data[] = [
                'category_id' => $faker->randomElement($categoryIds),
                'created_at' => $faker->dateTime(),
                'lat' => $faker->latitude(59.822019,60.038102),
                'lon' => $faker->longitude(30.095649, 30.53295)
            ];
        }
        DB::table('markers')->insert($data);
    }
}
